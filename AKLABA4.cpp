﻿#include <iostream>
#include <filesystem>
#include <vector>
#include <regex>
#include <string>
#include <fstream>
#include <cstdlib>

namespace fs = std::filesystem;

void printUsage() {
    std::cout << "Usage: program <directory> <pattern> [-i]\n"
        << "    <directory> : Directory to delete files from\n"
        << "    <pattern>   : File pattern to match (e.g., *.exe)\n"
        << "    -i          : Interactive mode\n";
}

bool confirmDeletion(const fs::path& filePath) {
    std::cout << "Do you really want to delete " << filePath << "? (y/n): ";
    char response;
    std::cin >> response;
    return response == 'y' || response == 'Y';
}

void deleteFiles(const std::string& directory, const std::string& pattern, bool interactive) {
    std::regex regexPattern(pattern);

    for (const auto& entry : fs::directory_iterator(directory)) {
        if (!fs::is_regular_file(entry)) continue;

        std::string fileName = entry.path().filename().string();
        if (!std::regex_match(fileName, regexPattern)) continue;

        // Check file attributes
        fs::perms filePerms = entry.status().permissions();

        if ((filePerms & fs::perms::owner_write) == fs::perms::none) {
            std::cout << "Skipping read-only file: " << entry.path() << "\n";
            continue;
        }

        if (interactive && !confirmDeletion(entry.path())) {
            continue;
        }

        try {
            fs::remove(entry.path());
            std::cout << "Deleted: " << entry.path() << "\n";
        }
        catch (const fs::filesystem_error& e) {
            std::cerr << "Error deleting file " << entry.path() << ": " << e.what() << "\n";
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        printUsage();
        return 1;
    }

    std::string directory = argv[1];
    std::string pattern = argv[2];
    bool interactive = false;

    if (argc == 4 && std::string(argv[3]) == "-i") {
        interactive = true;
    }

    if (!fs::exists(directory) || !fs::is_directory(directory)) {
        std::cerr << "Invalid directory: " << directory << "\n";
        return 2;
    }

    try {
        deleteFiles(directory, pattern, interactive);
    }
    catch (const std::exception& e) {
        std::cerr << "An error occurred: " << e.what() << "\n";
        return 3;
    }

    return 0;
}