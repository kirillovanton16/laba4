Інструкції з компіляції та запуску:

g++ -std=c++17 -o delete_files delete_files.cpp
./delete_files <directory> <pattern> [-i]

Приклад запуску:

./delete_files /path/to/directory "*.exe" -i
